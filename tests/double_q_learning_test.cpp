//
// Created by sebastian on 30.04.20.
//

#include <iostream>

#include "rl_cpp/double_q_learning.h"
#include "rl_cpp/grid_world.h"
#include "absl/strings/str_join.h"

using namespace rl_cpp;
using namespace rl_cpp::environment;


int main() {
    std::shared_ptr<grid_world::GridWorldEnvironment> env =
            std::make_shared<grid_world::GridWorldEnvironment>(3, 3);
    auto DoubleQLearning = algorithms::DoubleQLearning(env, algorithms::kGamma, 0.1, 1.0);
    DoubleQLearning(10000);
    std::cout << algorithms::PrintQ(DoubleQLearning.Q()) << std::endl;
    std::cout << algorithms::PrintPI(DoubleQLearning.PI()) << std::endl;
    std::cout << algorithms::PrintV(DoubleQLearning.V()) << std::endl;
}
