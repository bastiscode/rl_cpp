//
// Created by sebastian on 30.04.20.
//

#include "rl_cpp/monte_carlo_control.h"
#include "rl_cpp/grid_world.h"
#include "absl/strings/str_join.h"

using namespace rl_cpp;
using namespace rl_cpp::environment;

int main() {
    std::shared_ptr<Environment> env = std::make_shared<grid_world::GridWorldEnvironment>(3, 3);
    auto MCControl = algorithms::MonteCarloControl(env, algorithms::kGamma, 0.1);
    MCControl(10000);
    std::cout << algorithms::PrintQ(MCControl.Q()) << std::endl;
    std::cout << algorithms::PrintPI(MCControl.PI()) << std::endl;
    std::cout << algorithms::PrintV(MCControl.V()) << std::endl;
}
