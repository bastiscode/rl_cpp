//
// Created by sebastian on 25.04.20.
//

#include "rl_cpp/value_iteration.h"
#include "rl_cpp/grid_world.h"
#include "absl/strings/str_join.h"

using namespace rl_cpp;
using namespace rl_cpp::environment;

int main() {
    std::shared_ptr<Environment> env = std::make_shared<grid_world::GridWorldEnvironment>(4, 4);
    auto valueIteration = algorithms::ValueIteration(env);
    valueIteration();
    assert(valueIteration.V() ==
           std::vector<double>({0, -1, -2, -3, -1, -2, -3, -2, -2, -3, -2, -1, -3, -2, -1, 0}));
    std::cout << algorithms::PrintV(valueIteration.V()) << std::endl;
    assert(valueIteration.PI() == std::vector<std::vector<double>>({{1, 0, 0, 0},
                                                                    {0, 0, 0, 1},
                                                                    {0, 0, 0, 1},
                                                                    {0, 0, 1, 0},
                                                                    {1, 0, 0, 0},
                                                                    {1, 0, 0, 0},
                                                                    {1, 0, 0, 0},
                                                                    {0, 0, 1, 0},
                                                                    {1, 0, 0, 0},
                                                                    {1, 0, 0, 0},
                                                                    {0, 1, 0, 0},
                                                                    {0, 0, 1, 0},
                                                                    {1, 0, 0, 0},
                                                                    {0, 1, 0, 0},
                                                                    {0, 1, 0, 0},
                                                                    {1, 0, 0, 0}}));
}
