//
// Created by sebastian on 30.04.20.
//

//
// Created by sebastian on 30.04.20.
//

#include <chrono>

#include "absl/strings/str_format.h"
#include "rl_cpp/td_prediction.h"
#include "rl_cpp/grid_world.h"
#include "rl_cpp/value_iteration.h"

using namespace rl_cpp;
using namespace rl_cpp::environment;

int main() {
    int const width = 20;
    int const height = 20;
    std::shared_ptr<Environment> env = std::make_shared<grid_world::GridWorldEnvironment>(height, width);
    std::shared_ptr<Environment> envWithModel = std::make_shared<grid_world::GridWorldEnvironment>(height, width);
    auto valueIteration = algorithms::ValueIteration(envWithModel);
    valueIteration();
    auto TDPrediction = algorithms::TDPrediction(env, algorithms::kGamma, 1);
    auto const start = std::chrono::high_resolution_clock::now();
    TDPrediction(1000);
    std::cout << algorithms::PrintV(valueIteration.V()) << std::endl;
    std::cout << algorithms::PrintV(TDPrediction.V()) << std::endl;
    double const dur = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - start).count() / 1000.0;
    std::cout << absl::StrFormat("Took %.3f seconds", dur) << std::endl;
}

