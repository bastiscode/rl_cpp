//
// Created by sebastian on 30.04.20.
//

#include "rl_cpp/sarsa.h"
#include "rl_cpp/grid_world.h"
#include "absl/strings/str_join.h"

using namespace rl_cpp;
using namespace rl_cpp::environment;

int main() {
    std::shared_ptr<Environment> env = std::make_shared<grid_world::GridWorldEnvironment>(3, 3);
    auto SARSA = algorithms::SARSA(env, algorithms::kGamma, 0.1, 1.0);
    SARSA(1000);
    std::cout << algorithms::PrintQ(SARSA.Q()) << std::endl;
    std::cout << algorithms::PrintPI(SARSA.PI()) << std::endl;
    std::cout << algorithms::PrintV(SARSA.V()) << std::endl;
}
