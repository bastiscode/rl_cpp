//
// Created by sebastian on 30.04.20.
//

#include "rl_cpp/q_learning.h"
#include "rl_cpp/grid_world.h"
#include "absl/strings/str_join.h"

using namespace rl_cpp;
using namespace rl_cpp::environment;

int main() {
    std::shared_ptr<Environment> env = std::make_shared<grid_world::GridWorldEnvironment>(3, 3);
    auto QLearning = algorithms::QLearning(env, algorithms::kGamma, 0.1, 1.0);
    QLearning(1000);
    std::cout << algorithms::PrintQ(QLearning.Q()) << std::endl;
    std::cout << algorithms::PrintPI(QLearning.PI()) << std::endl;
    std::cout << algorithms::PrintV(QLearning.V()) << std::endl;
}
