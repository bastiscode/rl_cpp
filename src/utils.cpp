//
// Created by sebastian on 22.04.20.
//

#include "rl_cpp/utils.h"

int rl_cpp::utils::SampleAction(const std::vector<double> &distribution, double z) {
    // First do a check that this is indeed a proper discrete distribution.
    double sum = 0;
    for (auto const &p : distribution) {
        sum += p;
    }
    assert(sum == 1.0);
    // Now sample an outcome.
    sum = 0;
    for (int i = 0; i < distribution.size(); i++) {
        double const prob = distribution[i];
        if (sum <= z && z < (sum + prob)) {
            return i;
        }
        sum += prob;
    }
    return -1;
}

