//
// Created by sebastian on 30.04.20.
//

#include "rl_cpp/environment.h"
#include "rl_cpp/algorithms.h"
#include "rl_cpp/q_learning.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {
    void QLearning::Run() {
        Run(kMaxIterations);
    }

    void QLearning::Run(int numIterations) {
        int iterationCount_ = 0;
        while (iterationCount_ < numIterations) {
            auto state = env_->InitialState();
            while (true) {
                auto const action = EpsilonGreedy(Q_[state], epsilon_);
                auto const transitions = env_->SA(state, action);
                std::vector<double> transProbs;
                transProbs.reserve(transitions.size());
                for (auto const &t: transitions) {
                    transProbs.emplace_back(t.transitionProb);
                }
                auto const transition = transitions[Sample(transProbs)];
                auto const greedyNextAction = Greedy(Q_[transition.state]);

                Update(environment::TimeStep(
                        state,
                        action,
                        transition.reward,
                        transition.state,
                        transition.terminal),
                       greedyNextAction);

                if (transition.terminal) {
                    break;
                }
                state = transition.state;
            }
            iterationCount_++;
        }
    }

    void QLearning::Run(const environment::Episode &episode) {
        for (auto &timeStep: episode) {
            int greedyNextAction = Greedy(Q_[timeStep.nextState]);
            Update(timeStep, greedyNextAction);
        }
    }

    void QLearning::Update(const environment::TimeStep &timeStep, int nextAction) {
        Q_[timeStep.state][timeStep.action] = Q_[timeStep.state][timeStep.action] +
                                              alpha_ *
                                              (timeStep.reward + gamma_ *
                                                                 Q_[timeStep.nextState][nextAction] -
                                               Q_[timeStep.state][timeStep.action]);

        // update policy greedily
        auto PI_s = std::vector(env_->getNumActions(), 0.0);
        auto const bestA = Greedy(Q_[timeStep.state]);
        PI_s[bestA] = 1.0;
        PI_[timeStep.state] = PI_s;
    }

    const ValueFunction &QLearning::V() {
        for (int s = 0; s < env_->getNumStates(); s++) {
            double V_s = 0;
            for (int a = 0; a < env_->getNumActions(); a++) {
                V_s += Q_[s][a] * PI_[s][a];
            }
            V_[s] = V_s;
        }
        return V_;
    }
}
