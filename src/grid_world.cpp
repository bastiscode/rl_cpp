//
// Created by sebastian on 11.05.20.
//

#include <set>
#include <variant>
#include <unordered_map>

#include "rl_cpp/grid_world.h"
#include "absl/strings/str_format.h"

namespace rl_cpp::grid_world {
    GridWorldEnvironment::GridWorldEnvironment(int gridHeight, int gridWidth) :
            GridWorldEnvironment(gridHeight,
                                 gridWidth,
                                 std::vector<Goal>{
                                         Goal(0, 0),
                                         Goal(gridWidth * gridHeight - 1, 0)}) {}

    std::vector<environment::Transition> GridWorldEnvironment::SA(int state, int action) {
        return stateActionMatrix[state][action];
    }

    int GridWorldEnvironment::InitialState() {
        while (true) {
            int state = absl::Uniform(bitGen_, 0, numStates);
            // continue if state is a terminal state
            if (SA(state, 0).back().terminal) { continue; }
            return state;
        }
    }

    GridWorldEnvironment::GridWorldEnvironment(int gridHeight, int gridWidth,
                                               const std::vector<Goal> &goals,
                                               const std::vector<Wall> &walls,
//                                               const std::vector<Bridge> &bridges,
                                               const std::vector<Teleporter> &teleporters) :
            gridWidth_(gridWidth),
            gridHeight_(gridHeight),
            goals_(goals),
            walls_(walls),
//            bridges_(bridges),
            teleporters_(teleporters) {
        numStates = gridHeight * gridWidth;
        numActions = kNumActions; // UP, RIGHT, DOWN, LEFT
        // initialize transition matrix
        stateActionMatrix = std::vector(numStates,
                                        std::vector(numActions,
                                                    std::vector<environment::Transition>()));
        gridTileTypes_ = std::vector<GridTileType>(numStates, GridTileType::TILE);
        // helper data structure
        std::unordered_map<int, std::variant<Goal, Wall, Teleporter>> tileSpecs_;

        // process goals
        for (auto const &goal: goals) {
            for (int a = 0; a < numActions; a++) {
                stateActionMatrix[goal.state][a].emplace_back(1,
                                                              goal.state,
                                                              goal.rewardForReaching,
                                                              true);
            }
            tileSpecs_.insert({{goal.state, goal}});
            gridTileTypes_[goal.state] = GOAL;
        }
        // process walls
        for (auto const &wall : walls) {
            for (int a = 0; a < numActions; a++) {
                stateActionMatrix[wall.state][a].emplace_back(1,
                                                              wall.state,
                                                              0,
                                                              true);
            }
            tileSpecs_.insert({{wall.state, wall}});
            if (gridTileTypes_[wall.state] != TILE) {
                throw std::invalid_argument(
                        absl::StrFormat("Got multiple specifications for the same state %d", wall.state));
            }
            gridTileTypes_[wall.state] = WALL;
        }
        // process teleporters
        for (auto const &teleporter : teleporters) {
            switch (teleporter.type) {
                case UNIFORM:
                    // teleport to another state which is not a wall or the same teleporter state uniformly
                    // for each action
                    std::vector<environment::Transition> transitions;
                    for (int s = 0; s < numStates; s++) {
                        // dont teleport to same state
                        if (s == teleporter.state) {
                            continue;
                        }

                        bool terminal = false;
                        double reward = -1;

                        auto stateSpec = tileSpecs_.find(s);
                        if (stateSpec != tileSpecs_.end()) {
                            if (std::holds_alternative<Wall>(stateSpec->second)) {
                                // dont teleport to walls
                                continue;
                            } else if (std::holds_alternative<Goal>(stateSpec->second)) {
                                // if state teleported to is a goal state, change terminal and reward
                                const auto &goal = std::get<Goal>(stateSpec->second);
                                terminal = goal.terminal;
                                reward = goal.rewardForReaching;
                            }
                        }
                        // probability to land in a state is 1 / (#states - #walls - 1)
                        transitions.emplace_back(1.0 / (numStates - (int) walls.size() - 1),
                                                 s,
                                                 reward,
                                                 terminal
                        );
                    }
                    for (int a = 0; a < numActions; a++) {
                        stateActionMatrix[teleporter.state][a] = transitions;
                    }
                    break;
            }
            tileSpecs_.insert({{teleporter.state, teleporter}});
            if (gridTileTypes_[teleporter.state] != TILE) {
                throw std::invalid_argument(
                        absl::StrFormat("Got multiple specifications for the same state %d", teleporter.state));
            }
            gridTileTypes_[teleporter.state] = TELEPORTER;
        }
        for (int h = 0; h < gridHeight; h++) {
            for (int w = 0; w < gridWidth; w++) {
                auto const state = (h * gridWidth) + w;
                // skip if the state is a teleporter, wall or a goal, because we then already processed it
                if (tileSpecs_.find(state) != tileSpecs_.end()) {
                    continue;
                }

                auto neighborStates = getNeighboringStates(state);

                for (int a = 0; a < numActions; a++) {
                    int nextState = neighborStates[a];

                    double reward = -1;
                    bool terminal = false;

                    auto nextStateSpec = tileSpecs_.find(nextState);
                    if (nextStateSpec != tileSpecs_.end()) {
                        if (std::holds_alternative<Goal>(nextStateSpec->second)) {
                            // check if nextState would be a goal, then change reward and terminal flag
                            auto &goal = std::get<Goal>(nextStateSpec->second);
                            terminal = goal.terminal;
                            reward = goal.rewardForReaching;
                        } else if (std::holds_alternative<Wall>(nextStateSpec->second)) {
                            // check if next state would be a wall, then stay in the same state
                            nextState = state;
                        }
                    };

                    stateActionMatrix[state][a].emplace_back(1,
                                                             nextState,
                                                             reward,
                                                             terminal);
                }

                gridTileTypes_[state] = TILE;
            }
        }
        // process bridges
//        for (auto const &bridge : bridges) {
//            bool terminal = false;
//            double reward = -1;
//            // check if state the bridge goes to is a goal state
//            auto const goalIt = std::find_if(goals.begin(), goals.end(),
//                                             [bridge](Goal goal) { return goal.state == bridge.toState; });
//            if (goalIt != goals.end()) {
//                auto const goal = goals[goalIt - goals.begin()];
//                terminal = goal.terminal;
//                reward = goal.rewardForReaching;
//            }
//            // replace transitions to next states with bridge transition
//            stateActionMatrix[bridge.fromState][bridge.action].clear();
//            stateActionMatrix[bridge.fromState][bridge.action].emplace_back(
//                    1,
//                    bridge.toState,
//                    reward,
//                    terminal
//            );
//        }
    }

    int GridWorldEnvironment::getGridWidth() const {
        return gridWidth_;
    }

    int GridWorldEnvironment::getGridHeight() const {
        return gridHeight_;
    }

    GridStateInfo GridWorldEnvironment::StateInfo(int state) const {
        auto rowCol = rc(state);
        return GridStateInfo(std::get<0>(rowCol), std::get<1>(rowCol), gridTileTypes_[state]);
    }

    const std::vector<Goal> &GridWorldEnvironment::getGoals() const {
        return goals_;
    }

    const std::vector<Wall> &GridWorldEnvironment::getWalls() const {
        return walls_;
    }

//    const std::vector<Bridge> &GridWorldEnvironment::getBridges() const {
//        return bridges_;
//    }

    const std::vector<Teleporter> &GridWorldEnvironment::getTeleporters() const {
        return teleporters_;
    }

//    bool GridWorldEnvironment::setGridTile(int state, const std::variant<Tile, Goal, Wall, Teleporter> &gridTile) {
//        assert(state >= 0 && state < numStates);
//        if (std::holds_alternative<Tile>(gridTile)) {
//            Tile tile = std::get<0>(gridTile);
//            return setTile(state, tile);
//        } else if (std::holds_alternative<Goal>(gridTile)) {
//            Goal goal = std::get<1>(gridTile);
//            return setGoal(state, goal);
//        } else if (std::holds_alternative<Wall>(gridTile)) {
//            Wall wall = std::get<2>(gridTile);
//            return setWall(state, wall);
//        } else if (std::holds_alternative<Teleporter>(gridTile)) {
//            Teleporter teleporter = std::get<3>(gridTile);
//            return setTeleporter(state, teleporter);
//        }
//        // should never happen
//        return false;
//    }

    std::vector<int> GridWorldEnvironment::getNeighboringStates(int state) {
        auto rowCol = rc(state);
        int row = std::get<0>(rowCol);
        int col = std::get<1>(rowCol);
        int upNeighbor = row == 0 ? state : state - gridWidth_;
        int rightNeighbor = col == gridWidth_ - 1 ? state : state + 1;
        int downNeighbor = row == gridHeight_ - 1 ? state : state + gridWidth_;
        int leftNeighbor = col == 0 ? state : state - 1;
        return std::vector<int>({upNeighbor, rightNeighbor, downNeighbor, leftNeighbor});
    }

    std::tuple<int, int> GridWorldEnvironment::rc(int state) const {
        auto const row = floor((double) state / gridWidth_);
        auto const col = state - row * gridWidth_;
        return {row, col};
    }

//    bool GridWorldEnvironment::setTile(int state, Tile tile) {
//        auto neighbors = getNeighboringStates(state);
//        std::vector<environment::Transition> transitions;
//        transitions.reserve(numActions);
//        for (auto & neighbor : neighbors) {
//            // happens for tiles on edges of the board
//            if (neighbor == state) {
//                transitions.emplace_back(1.0, state, -1, false);
//                continue;
//            }
//
//            // else we have to check for the type of the neighbor
//            auto neighborType = gridTileTypes_[neighbor];
//
//            transitions.emplace_back()
//        }
//        gridTileTypes_[state] = TILE;
//        return false;
//    }
//
//    bool GridWorldEnvironment::setGoal(int state, Goal goal) {
//        auto neighbors = getNeighboringStates(state);
//        gridTileTypes_[state] = GOAL;
//        return false;
//    }
//
//    bool GridWorldEnvironment::setWall(int state, Wall wall) {
//        auto neighbors = getNeighboringStates(state);
//        gridTileTypes_[state] = WALL;
//        return false;
//    }
//
//    bool GridWorldEnvironment::setTeleporter(int state, Teleporter teleporter) {
//        auto neighbors = getNeighboringStates(state);
//        gridTileTypes_[state] = TELEPORTER;
//        return false;
//    }
}
