//
// Created by sebastian on 30.04.20.
//

#include "rl_cpp/environment.h"
#include "rl_cpp/algorithms.h"
#include "rl_cpp/sarsa.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    void SARSA::Run() {
        Run(kMaxIterations);
    }

    void SARSA::Run(int numIterations) {
        int iterationCount_ = 0;
        while (iterationCount_ < numIterations) {
            auto state = env_->InitialState();
            auto action = EpsilonGreedy(Q_[state], epsilon_);
            while (true) {
                auto const transitions = env_->SA(state, action);
                std::vector<double> transProbs;
                transProbs.reserve(transitions.size());
                for (auto const &t: transitions) {
                    transProbs.emplace_back(t.transitionProb);
                }
                auto const transition = transitions[Sample(transProbs)];
                auto const nextAction = EpsilonGreedy(Q_[transition.state], epsilon_);

                Update(environment::TimeStep(
                        state,
                        action,
                        transition.reward,
                        transition.state,
                        transition.terminal),
                       nextAction);

                if (transition.terminal) {
                    break;
                }
                state = transition.state;
                action = nextAction;
            }
            iterationCount_++;
        }
    }

    void SARSA::Run(const environment::Episode &episode) {
        int nextActionIdx = 1;
        for (auto &timeStep: episode) {
            int nextAction;
            if (nextActionIdx >= episode.size()) {
                nextAction = EpsilonGreedy(Q_[timeStep.nextState], epsilon_);
            } else {
                nextAction = episode[nextActionIdx].action;
            }
            Update(timeStep, nextAction);
            nextActionIdx++;
        }
    }

    void SARSA::Update(const environment::TimeStep &timeStep, int nextAction) {
        Q_[timeStep.state][timeStep.action] = Q_[timeStep.state][timeStep.action] +
                                              alpha_ *
                                              (timeStep.reward + gamma_ *
                                                                 Q_[timeStep.nextState][nextAction] -
                                               Q_[timeStep.state][timeStep.action]);

        // update policy greedily
        auto PI_s = std::vector(env_->getNumActions(), 0.0);
        auto const bestA = Greedy(Q_[timeStep.state]);
        PI_s[bestA] = 1.0;
        PI_[timeStep.state] = PI_s;
    }

    const ValueFunction& SARSA::V() {
        for (int s = 0; s < env_->getNumStates(); s++) {
            double V_s = 0;
            for (int a = 0; a < env_->getNumActions(); a++) {
                V_s += Q_[s][a] * PI_[s][a];
            }
            V_[s] = V_s;
        }
        return V_;
    }
}
