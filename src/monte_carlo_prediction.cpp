//
// Created by sebastian on 29.04.20.
//


#include <absl/strings/str_format.h>

#include "rl_cpp/environment.h"
#include "rl_cpp/algorithms.h"
#include "rl_cpp/monte_carlo_prediction.h"


using namespace rl_cpp;

namespace rl_cpp::algorithms {

    void MonteCarloPrediction::Run() {
        Run(kMaxIterations);
    }

    void MonteCarloPrediction::Run(int numIterations) {
        int iterationCount_ = 0;
        while (iterationCount_ < numIterations) {
            auto const episode = GenerateEpisode();
            Run(episode);
            iterationCount_++;
        }
    }

    environment::Episode MonteCarloPrediction::GenerateEpisode() {
        environment::Episode episode;
        int state = env_->InitialState();
        while (true) {
            auto const prob = PI_[state];
            auto const action = algorithms::Sample(prob);
            auto const transitions = env_->SA(state, action);
            std::vector<double> transProbs;
            transProbs.reserve(transitions.size());
            for (auto const &t: transitions) {
                transProbs.emplace_back(t.transitionProb);
            }
            auto const transition = transitions[Sample(transProbs)];
            episode.emplace_back(state, action, transition.reward, transition.state, transition.terminal);
            if (transition.terminal) {
                break;
            }
            state = transition.state;
        }
        return episode;
    }

    std::vector<double> MonteCarloPrediction::AvgReturns() {
        std::vector<double> avgReturns(avgReturns_.size());
        for (auto const &r : avgReturns_) {
            avgReturns.emplace_back(r.first);
        }
        return avgReturns;
    }

    void MonteCarloPrediction::Run(const environment::Episode &episode) {
        double G = 0;
        // go backwards through the episode
        for (int t = (int) episode.size() - 1; t >= 0; t--) {
            auto const timestep = episode[t];
            G = gamma_ * G + timestep.reward;
            if (predictionType_ == MonteCarloType::FirstVisit && t > 0) {
                auto const it = std::find_if(episode.begin(), episode.begin() + (t - 1),
                                             [timestep](environment::TimeStep t) {
                                                 return timestep.state == t.state;
                                             });
                if (it != episode.begin() + (t - 1)) {
                    continue;
                }
            }
            Returns_[timestep.state].emplace_back(G);
            // increase state visitation counter
            avgReturns_[timestep.state].second++;
            avgReturns_[timestep.state].first +=
                    (1.0 / avgReturns_[timestep.state].second) * (G - avgReturns_[timestep.state].first);
            V_[timestep.state] = avgReturns_[timestep.state].first;
        }
    }
}

