//
// Created by sebastian on 25.04.20.
//

#include "rl_cpp/algorithms.h"
#include "rl_cpp/value_iteration.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {
    void ValueIteration::PolicyEvaluation() {
        while (true) {
            double delta = 0;
            for (auto s = 0; s < V_.size(); s++) {
                auto vOld = V_[s];
                std::vector<double> Q_s(env_->getNumActions(), 0);
                for (int a = 0; a < env_->getNumActions(); a++) {
                    auto const transitions = env_->SA(s, a);
                    for (auto const &t : transitions) {
                        Q_s[a] += t.transitionProb * (t.reward + (1 - t.terminal) * gamma_ * V_[t.state]);
                    }
                }
                auto bestQ = std::distance(Q_s.begin(), std::max_element(Q_s.begin(), Q_s.end()));
                delta = std::max(delta, std::abs(vOld - Q_s[bestQ]));
                V_[s] = Q_s[bestQ];
            }
            if (delta < maxDelta_) {
                break;
            }
        }
    }

    void ValueIteration::PolicyExtraction() {
        for (auto s = 0; s < PI_.size(); s++) {
            auto const PI_s = PI_[s];
            std::vector<double> A(env_->getNumActions(), 0);
            for (int a = 0; a < env_->getNumActions(); a++) {
                auto const transitions = env_->SA(s, a);
                for (auto const &t : transitions) {
                    A[a] += t.transitionProb * (t.reward + (1 - t.terminal) * gamma_ * V_[t.state]);
                }
            }
            auto bestA = std::distance(A.begin(), std::max_element(A.begin(), A.end()));
            auto updatedPI_s = std::vector<double>(env_->getNumActions(), 0);
            updatedPI_s[bestA] = 1.0;
            PI_[s] = updatedPI_s;
        }
    }

    void ValueIteration::Run() {
        PolicyEvaluation();
        PolicyExtraction();
    }

    void ValueIteration::Run(int numIterations) {
        PolicyEvaluation();
        PolicyExtraction();
    }
}