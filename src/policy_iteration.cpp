//
// Created by sebastian on 22.04.20.
//

#include "rl_cpp/policy_iteration.h"
#include "rl_cpp/utils.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    void PolicyIteration::PolicyEvaluation() {
        while (true) {
            double delta = 0;
            for (auto s = 0; s < V_.size(); s++) {
                auto vOld = V_[s];
                double V_s = 0;
                for (int a = 0; a < env_->getNumActions(); a++) {
                    auto const transitions = env_->SA(s, a);
                    for (auto const &t : transitions) {
                        V_s += PI_[s][a] * t.transitionProb * (t.reward + (1 - t.terminal) * gamma_ * V_[t.state]);
                    }
                }
                delta = std::max(delta, std::abs(vOld - V_s));
                V_[s] = V_s;
            }
            if (delta < maxDelta_) {
                break;
            }
        }
    }

    bool PolicyIteration::PolicyImprovement() {
        bool stable = true;
        for (auto s = 0; s < PI_.size(); s++) {
            auto const PI_s = PI_[s];
            auto const bestAOld = std::distance(PI_s.begin(),
                                                std::max_element(PI_s.begin(), PI_s.end()));
            std::vector<double> A(env_->getNumActions(), 0);
            for (int a = 0; a < env_->getNumActions(); a++) {
                auto const transitions = env_->SA(s, a);
                for (auto const &t : transitions) {
                    A[a] += t.transitionProb * (t.reward + (1 - t.terminal) * gamma_ * V_[t.state]);
                }
            }
            auto bestA = std::distance(A.begin(), std::max_element(A.begin(), A.end()));
            if (bestAOld != bestA) {
                stable = false;
            }
            auto updatedPI_s = std::vector<double>(env_->getNumActions(), 0);
            updatedPI_s[bestA] = 1.0;
            PI_[s] = updatedPI_s;
        }
        return stable;
    }

    void PolicyIteration::Run() {
        auto iteration = 0;
        while (true) {
            PolicyEvaluation();
            auto const stable = PolicyImprovement();
            iteration++;
            if (stable || iteration >= maxIterations_) {
                break;
            }
        }
    }

    void PolicyIteration::Run(int numIterations) {
        auto iteration = 0;
        while (true) {
            PolicyEvaluation();
            auto const stable = PolicyImprovement();
            iteration++;
            if (stable || iteration >= numIterations) {
                break;
            }
        }
    }
}
