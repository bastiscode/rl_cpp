//
// Created by sebastian on 30.04.20.
//

#include <absl/strings/str_format.h>

#include "rl_cpp/environment.h"
#include "rl_cpp/algorithms.h"
#include "rl_cpp/monte_carlo_control.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {
    std::vector<std::vector<double>> MonteCarloControl::AvgReturns() {
        std::vector<std::vector<double>> avgReturns;
        avgReturns.reserve(env_->getNumStates());
        for (int s = 0; s < env_->getNumStates(); s++) {
            avgReturns[s].reserve(env_->getNumActions());
            for (int a = 0; a < env_->getNumActions(); a++) {
                avgReturns[s][a] = avgReturns_[s][a].first;
            }
        }
        return avgReturns;
    }

    environment::Episode MonteCarloControl::GenerateEpisode() {
        environment::Episode episode;
        int state = env_->InitialState();
        while (true) {
            auto const prob = PI_[state];
            auto const action = algorithms::Sample(prob);
            auto const transitions = env_->SA(state, action);
            std::vector<double> transProbs;
            transProbs.reserve(transitions.size());
            for (auto const &t: transitions) {
                transProbs.emplace_back(t.transitionProb);
            }
            auto const transition = transitions[Sample(transProbs)];
            episode.emplace_back(state, action, transition.reward, transition.state, transition.terminal);
            if (transition.terminal) {
                break;
            }
            state = transition.state;
        }
        return episode;
    }

    void MonteCarloControl::Run() {
        Run(kMaxIterations);
    }

    void MonteCarloControl::Run(int numIterations) {
        int iterationCount_ = 0;
        while (iterationCount_ < numIterations) {
            auto const episode = GenerateEpisode();
            Run(episode);
            iterationCount_++;
        }
    }

    const ValueFunction& MonteCarloControl::V() {
        for (int s = 0; s < env_->getNumStates(); s++) {
            double V_s = 0;
            for (int a = 0; a < env_->getNumActions(); a++) {
                V_s += Q_[s][a] * PI_[s][a];
            }
            V_[s] = V_s;
        }
        return V_;
    }

    void MonteCarloControl::Run(const environment::Episode &episode) {
        double G = 0;
        // go backwards through the episode
        for (int t = (int) episode.size() - 1; t >= 0; t--) {
            auto const transition = episode[t];
            G = gamma_ * G + transition.reward;
            if (controlType_ == MonteCarloType::FirstVisit && t > 0) {
                auto const it = std::find_if(episode.begin(), episode.begin() + (t - 1),
                                             [transition](environment::TimeStep t) {
                                                 return transition.state == t.state &&
                                                        transition.action == t.action;
                                             });
                if (it != episode.begin() + (t - 1)) {
                    continue;
                }
            }
            Returns_[transition.state][transition.action].emplace_back(G);
            // increase state visitation counter
            avgReturns_[transition.state][transition.action].second++;
            avgReturns_[transition.state][transition.action].first +=
                    (1.0 / avgReturns_[transition.state][transition.action].second) *
                    (G - avgReturns_[transition.state][transition.action].first);
            Q_[transition.state][transition.action] = avgReturns_[transition.state][transition.action].first;

            std::vector<double> A(env_->getNumActions(), 0);
            auto bestA = std::distance(Q_[transition.state].begin(),
                                       std::max_element(Q_[transition.state].begin(), Q_[transition.state].end()));
            for (int a = 0; a < A.size(); a++) {
                if (a == bestA) {
                    A[a] = 1 - epsilon_ + (epsilon_ / A.size());
                } else {
                    A[a] = (epsilon_ / A.size());
                }
            }
            PI_[transition.state] = A;
        }
    }
}
