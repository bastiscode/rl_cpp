//
// Created by sebastian on 29.04.20.
//

#include "rl_cpp/environment.h"
#include "rl_cpp/algorithms.h"
#include "rl_cpp/td_prediction.h"


using namespace rl_cpp;

namespace rl_cpp::algorithms {
    void TDPrediction::Run() {
        Run(kMaxIterations);
    }

    void TDPrediction::Run(int numIterations) {
        int iterationCount_ = 0;
        while (iterationCount_ < numIterations) {
            auto state = env_->InitialState();
            while (true) {
                auto const action = Sample(PI_[state]);
                auto const transitions = env_->SA(state, action);
                std::vector<double> transProbs;
                transProbs.reserve(transitions.size());
                for (auto const &t: transitions) {
                    transProbs.emplace_back(t.transitionProb);
                }
                auto const transition = transitions[Sample(transProbs)];
                V_[state] = V_[state] + alpha_ * (transition.reward + gamma_ * V_[transition.state] - V_[state]);
                if (transition.terminal) {
                    break;
                }
                state = transition.state;
            }
            iterationCount_++;
        }
    }
}

