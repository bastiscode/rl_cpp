//
// Created by sebastian on 30.04.20.
//

#ifndef SEB_RL_CPP_MONTE_CARLO_CONTROL_H
#define SEB_RL_CPP_MONTE_CARLO_CONTROL_H

#include <utility>

#include "environment.h"
#include "algorithms.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class MonteCarloControl : public Algorithm {
        inline static const std::string name = "MonteCarloControl";
        inline static const std::string description = "MonteCarloControl is an on-policy model-free algorithm. "
                                                      "It incrementally updates a Q-function towards complete episode "
                                                      "returns therefore being a Monte-Carlo method.";
        inline static const std::set<Specifications> specifications = {Specifications::CONTROL,
                                                                       Specifications::MODELFREE,
                                                                       Specifications::MONTECARLO,
                                                                       Specifications::ONPOLICY};
        inline static const bool providesValueFunction = false;
        inline static const bool providesPolicy = true;
        inline static const bool providesQFunction = true;

    public:
        explicit MonteCarloControl(std::shared_ptr<environment::Environment> environment,
                                   double gamma = kGamma,
                                   double epsilon = kEpsilon,
                                   MonteCarloType controlType = MonteCarloType::FirstVisit) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications),
                epsilon_(epsilon),
                controlType_(controlType) {
            // initialize returns with an empty vector for each state-action pair
            // Returns_ = std::vector(env_->getNumStates(), std::vector(env_->getNumActions(), std::vector<double>{}));
            Returns_.reserve(env_->getNumStates());
            for (int i = 0; i < env_->getNumStates(); i++) {
                std::vector<std::vector<double>> returnVecs;
                returnVecs.reserve(env_->getNumActions());
                for (int j = 0; j < env_->getNumActions(); j++) {
                    returnVecs.emplace_back(0, 0);
                }
                Returns_.emplace_back(returnVecs);
            }

            // initialize avg returns
            avgReturns_.reserve(env_->getNumStates());
            for (int s = 0; s < env_->getNumStates(); s++) {
                avgReturns_[s].reserve(env_->getNumActions());
                for (int a = 0; a < env_->getNumActions(); a++) {
                    avgReturns_[s][a] = std::pair<double, int>(0, 0);
                }
            }
        };

        [[nodiscard]] const ValueFunction& V() override;

        std::vector<std::vector<double>> AvgReturns();

        environment::Episode GenerateEpisode();

    private:
        void Run() override;

        void Run(int numIterations) override;

        void Run(const environment::Episode &episode) override;

        double epsilon_;
        MonteCarloType controlType_;
        std::vector<Returns> Returns_;
        std::vector<std::vector<std::pair<double, int>>> avgReturns_;
    };
}

#endif //SEB_RL_CPP_MONTE_CARLO_CONTROL_H
