//
// Created by sebastian on 29.04.20.
//

#ifndef SEB_RL_CPP_MONTE_CARLO_PREDICTION_H
#define SEB_RL_CPP_MONTE_CARLO_PREDICTION_H

#include <utility>

#include "environment.h"
#include "algorithms.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class MonteCarloPrediction : public Algorithm {
        inline static const std::string name = "MonteCarloPrediction";
        inline static const std::string description = "MonteCarloPrediction can be used to evaluate a given policy "
                                                      "by learning the corresponding value function from "
                                                      "complete episodes.";
        inline static const std::set<Specifications> specifications = {Specifications::PREDICTION,
                                                                       Specifications::MODELFREE,
                                                                       Specifications::MONTECARLO};
        inline static const bool providesValueFunction = true;
        inline static const bool providesPolicy = false;
        inline static const bool providesQFunction = false;

    public:
        explicit MonteCarloPrediction(std::shared_ptr<environment::Environment> environment,
                                      Policy PI,
                                      double gamma = kGamma,
                                      MonteCarloType predictionType = MonteCarloType::FirstVisit) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications),
                predictionType_(predictionType) {
            PI_ = std::move(PI);

            // initialize returns with an empty vector for each state
            Returns_ = Returns(env_->getNumStates(), std::vector<double>{});

            // initialize avg returns
            for (int s = 0; s < env_->getNumStates(); s++) {
                avgReturns_.emplace_back(0, 0);
            }
        };

        explicit MonteCarloPrediction(std::shared_ptr<environment::Environment> environment,
                                      double gamma = kGamma,
                                      MonteCarloType predictionType = MonteCarloType::FirstVisit) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications), predictionType_(predictionType) {
            // initialize returns with an empty vector for each state
            Returns_ = Returns(env_->getNumStates(), std::vector<double>{});

            // initialize avg returns
            for (int s = 0; s < env_->getNumStates(); s++) {
                avgReturns_.emplace_back(0, 0);
            }
        };

        std::vector<double> AvgReturns();

        environment::Episode GenerateEpisode();

    private:

        void Run() override;

        void Run(int numIterations) override;

        void Run(const environment::Episode &episode) override;

        MonteCarloType predictionType_;
        Returns Returns_;
        std::vector<std::pair<double, int>> avgReturns_;
    };
}

#endif //SEB_RL_CPP_MONTE_CARLO_PREDICTION_H
