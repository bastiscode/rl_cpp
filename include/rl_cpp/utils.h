//
// Created by sebastian on 22.04.20.
//

#ifndef SEB_RL_CPP_UTILS_H
#define SEB_RL_CPP_UTILS_H

#include <utility>
#include <vector>
#include <cassert>

namespace rl_cpp::utils {
    int SampleAction(const std::vector<double> &distribution,
                     double z);
}

#endif //SEB_RL_CPP_UTILS_H
