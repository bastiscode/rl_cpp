//
// Created by sebastian on 30.04.20.
//

#ifndef SEB_RL_CPP_Q_LEARNING_H
#define SEB_RL_CPP_Q_LEARNING_H

#include <utility>

#include "environment.h"
#include "algorithms.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class QLearning : public Algorithm {
        inline static const std::string name = "QLearning";
        inline static const std::string description = "QLearning is a model-free off-policy temporal-difference method. "
                                                      "It learns a Q-function which maps state-action pairs to values "
                                                      "where the values correspond to the expected return one gets when"
                                                      "choosing the action in the state and following "
                                                      "a greedy policy afterwards.";
        inline static const std::set<Specifications> specifications = {Specifications::CONTROL,
                                                                       Specifications::MODELFREE,
                                                                       Specifications::TEMPORALDIFFERENCE,
                                                                       Specifications::OFFPOLICY};
        inline static const bool providesValueFunction = false;
        inline static const bool providesPolicy = true;
        inline static const bool providesQFunction = true;

    public:
        explicit QLearning(std::shared_ptr<environment::Environment> environment,
                           double gamma = kGamma,
                           double epsilon = kEpsilon,
                           double alpha = 0.1) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications), epsilon_(epsilon),
                alpha_(alpha) {};

        const ValueFunction &V() override;

    private:
        void Run() override;

        void Run(int numIterations) override;

        void Run(const environment::Episode &episode) override;

        void Update(const environment::TimeStep &timeStep, int nextAction);

        double epsilon_;
        double alpha_;
    };
}

#endif //SEB_RL_CPP_Q_LEARNING_H
