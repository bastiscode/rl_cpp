//
// Created by sebastian on 22.04.20.
//

#ifndef SEB_RL_CPP_POLICY_ITERATION_H
#define SEB_RL_CPP_POLICY_ITERATION_H

#include "environment.h"
#include "algorithms.h"

#include "absl/strings/str_join.h"
#include <memory>
#include <utility>

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class PolicyIteration : public Algorithm {
        inline static const std::string name = "PolicyIteration";
        inline static const std::string description = "PolicyIteration alternately evaluates the current policy to get a value function (policy evaluation) and "
                                                      "then improves the policy using this value function (policy improvement). "
                                                      "We can do this because we have a model of the environment. "
                                                      "If you dont have a model you would need to use other algorithms which can "
                                                      "extract a policy from Q-values in the model-free way.";
        inline static const std::set<Specifications> specifications = {Specifications::CONTROL,
                                                                       Specifications::MODELBASED};
        inline static const bool providesValueFunction = true;
        inline static const bool providesPolicy = true;
        inline static const bool providesQFunction = false;

    public:
        explicit PolicyIteration(std::shared_ptr<environment::Environment> environment,
                                 double gamma = kGamma,
                                 int maxIterations = kMaxIterations,
                                 double maxDelta = kMaxDelta) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications),
                maxIterations_(maxIterations),
                maxDelta_(maxDelta) {};

    private:
        void PolicyEvaluation();

        bool PolicyImprovement();

        void Run() override;

        void Run(int numIterations) override;

        double maxDelta_;
        int maxIterations_;
    };
}
#endif //SEB_RL_CPP_POLICY_ITERATION_H
