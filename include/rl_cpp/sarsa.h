//
// Created by sebastian on 30.04.20.
//

#ifndef SEB_RL_CPP_SARSA_H
#define SEB_RL_CPP_SARSA_H

#include <utility>

#include "environment.h"
#include "algorithms.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class SARSA : public Algorithm {
        inline static const std::string name = "SARSA";
        inline static const std::string description = "SARSA is an model-free on-policy temporal-difference algorithm. "
                                                      "It learns a Q-function similar to QLearning but since it is "
                                                      "on-policy it optimizes the policy which it uses to go through "
                                                      "the environment.";
        inline static const std::set<Specifications> specifications = {Specifications::CONTROL,
                                                                       Specifications::MODELFREE,
                                                                       Specifications::TEMPORALDIFFERENCE,
                                                                       Specifications::ONPOLICY};
        inline static const bool providesValueFunction = false;
        inline static const bool providesPolicy = true;
        inline static const bool providesQFunction = true;

    public:
        explicit SARSA(std::shared_ptr<environment::Environment> environment,
                       double gamma = kGamma,
                       double epsilon = kEpsilon,
                       double alpha = 0.1) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications),
                epsilon_(epsilon),
                alpha_(alpha) {};

        const ValueFunction &V() override;

    private:
        void Run() override;

        void Run(int numIterations) override;

        void Run(const environment::Episode &episode) override;

        void Update(const environment::TimeStep &timeStep, int nextAction);

        double epsilon_;
        double alpha_;
    };
}

#endif //SEB_RL_CPP_SARSA_H
