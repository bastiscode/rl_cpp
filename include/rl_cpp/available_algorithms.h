//
// Created by sebastian on 24.05.20.
//

#ifndef SEB_RL_CPP_AVAILABLE_ALGORITHMS_H
#define SEB_RL_CPP_AVAILABLE_ALGORITHMS_H

#include "algorithms.h"
#include "grid_world.h"
#include "value_iteration.h"
#include "policy_iteration.h"
#include "q_learning.h"
#include "double_q_learning.h"
#include "sarsa.h"
#include "td_prediction.h"
#include "monte_carlo_control.h"
#include "monte_carlo_prediction.h"


namespace rl_cpp::algorithms {
    static std::vector<AlgorithmInfo> AvailableAlgorithms() {
        auto dummyEnv = std::make_shared<grid_world::GridWorldEnvironment>(2, 2);
        auto dummyPolicy = UniformRandomPolicy(dummyEnv->getNumStates(), dummyEnv->getNumActions());

        std::vector<AlgorithmInfo> availableAlgorithms;

        availableAlgorithms.emplace_back(ValueIteration(dummyEnv).Info());
        availableAlgorithms.emplace_back(PolicyIteration(dummyEnv).Info());
        availableAlgorithms.emplace_back(QLearning(dummyEnv).Info());
        availableAlgorithms.emplace_back(DoubleQLearning(dummyEnv).Info());
        availableAlgorithms.emplace_back(SARSA(dummyEnv).Info());
        availableAlgorithms.emplace_back(MonteCarloControl(dummyEnv).Info());
        availableAlgorithms.emplace_back(TDPrediction(dummyEnv, dummyPolicy).Info());
        availableAlgorithms.emplace_back(MonteCarloPrediction(dummyEnv, dummyPolicy).Info());

        return availableAlgorithms;
    };
}

#endif //SEB_RL_CPP_AVAILABLE_ALGORITHMS_H
