//
// Created by sebastian on 25.04.20.
//

#ifndef SEB_RL_CPP_ALGORITHMS_H
#define SEB_RL_CPP_ALGORITHMS_H

#include "environment.h"
#include <utility>
#include <vector>
#include <map>
#include <set>
#include <any>
#include <iostream>
#include <absl/strings/str_join.h>
#include <absl/random/random.h>

namespace rl_cpp::algorithms {
    constexpr double kGamma = 1.0;
    constexpr int kMaxIterations = 1000;
    constexpr double kMaxDelta = 0.000001;
    constexpr double kEpsilon = 0.1;

    using ValueFunction = std::vector<double>;
    using QFunction = std::vector<std::vector<double>>;
    using Policy = std::vector<std::vector<double>>;
    using Returns = std::vector<std::vector<double>>;

    static Policy UniformRandomPolicy(int numStates, int numActions) {
        std::vector<std::vector<double>> PI_(numStates, std::vector<double>(numActions, 1.0 / numActions));
        return PI_;
    }

    enum Specifications {
        MODELBASED,
        MODELFREE,
        OFFPOLICY,
        ONPOLICY,
        PREDICTION,
        CONTROL,
        MONTECARLO,
        TEMPORALDIFFERENCE,
        LAMDA
    };

    struct AlgorithmInfo {
        std::string name;
        std::string description;
        std::set<Specifications> specifications;
        bool providesQFunction;
        bool providesValueFunction;
        bool providesPolicy;

        AlgorithmInfo(
                std::string name, std::string description, std::set<Specifications> specifications,
                bool providesQFunction, bool providesValueFunction, bool providesPolicy) :
                name(std::move(name)), description(std::move(description)),
                specifications(std::move(specifications)),
                providesQFunction(providesQFunction), providesValueFunction(providesValueFunction),
                providesPolicy(providesPolicy) {};
    };

    struct AlgorithmStateInfo {
        double value;
        std::vector<double> qValues;
        std::vector<double> policy;

        AlgorithmStateInfo(double value, const std::vector<double> &qValues, const std::vector<double> &policy) :
                value(value) {
            this->qValues = qValues;
            this->policy = policy;
        };
    };

    class Algorithm {
    public:
        virtual ~Algorithm() = default;

        Algorithm(std::shared_ptr<environment::Environment> env,
                  double gamma,
                  std::string name,
                  std::string description,
                  bool providesValueFunction,
                  bool providesPolicy,
                  bool providesQFunction,
                  std::set<Specifications> specifications) :
                env_(std::move(env)),
                gamma_(gamma),
                name_(std::move(name)),
                description_(std::move(description)),
                providesPolicy(providesPolicy),
                providesValueFunction(providesValueFunction),
                providesQFunction(providesQFunction),
                specifications_(std::move(specifications)) {
            // initialize v and q with zeros
            V_ = std::vector<double>(env_->getNumStates(), 0.0);
            // Q_ = std::vector(env_->getNumStates(),
            //      std::vector<double>(env_->getNumActions(), 0.0));
            // have to do this initialization instead of the two lines above because of deduction issues
            // for use with QML WebAssembly
            Q_.reserve(env_->getNumStates());
            for (int i = 0; i < env_->getNumStates(); i++) {
                Q_.emplace_back(env_->getNumActions(), 0.0);
            }
            // initialize policy uniformly
            PI_ = UniformRandomPolicy(env_->getNumStates(), env_->getNumActions());
        };

        virtual void InitQ(const QFunction &Q) {
            if (Q.size() != Q_.size()) {
                throw std::invalid_argument("Sizes of q functions don't match");
            }
            Q_ = Q;
        }

        virtual void InitPI(const Policy &PI) {
            if (PI.size() != PI_.size()) {
                throw std::invalid_argument("Sizes of policies don't match");
            }
            PI_ = PI;
        }

        virtual void InitV(const ValueFunction &V) {
            if (V.size() != V_.size()) {
                throw std::invalid_argument("Sizes of value functions don't match");
            }
            V_ = V;
        }

        void operator()() {
            Run();
        };

        void operator()(int numIterations) {
            Run(numIterations);
        }

        void operator()(const environment::Episode &episode) {
            // episode can only have a terminal state in the end
            for (int i = 0; i < episode.size(); i++) {
                if (episode[i].terminal && i < episode.size() - 1) {
                    throw std::invalid_argument("Found a terminal state that is not at the end of the episode");
                }
            }
            // can only run on episodes with monte carlo or td algorithms
            if (std::find(specifications_.begin(), specifications_.end(), MONTECARLO) != specifications_.end()) {
                // last step has to be terminal in the monte carlo case
                if (!episode.back().terminal) {
                    throw std::invalid_argument("Monte-Carlo methods can only run on complete episodes");
                }
                Run(episode);
            } else if (std::find(specifications_.begin(), specifications_.end(), TEMPORALDIFFERENCE) !=
                       specifications_.end()) {
                Run(episode);
            }
            throw std::invalid_argument("Only Monte-Carlo and Temporal-Difference methods can run on episodes");
        }

        [[nodiscard]] virtual const ValueFunction &V() {
            return V_;
        }

        [[nodiscard]] virtual const Policy &PI() {
            return PI_;
        }

        [[nodiscard]] virtual const QFunction &Q() {
            return Q_;
        }

        AlgorithmStateInfo StateInfo(int state) {
            return AlgorithmStateInfo(V()[state],
                                      Q()[state],
                                      PI()[state]);
        }

        AlgorithmInfo Info() {
            return AlgorithmInfo(name_, description_, specifications_,
                                 providesQFunction, providesValueFunction, providesPolicy);
        };

        bool providesValueFunction;
        bool providesPolicy;
        bool providesQFunction;

    protected:

        virtual void Run() = 0;

        virtual void Run(int numIterations) = 0;

        virtual void Run(const environment::Episode &episode) {

        }

        ValueFunction V_;
        QFunction Q_;
        Policy PI_;
        double gamma_;
        std::shared_ptr<environment::Environment> env_;
        std::set<Specifications> specifications_;
        std::string name_;
        std::string description_;
    };

    static std::string PrintV(const ValueFunction &V) {
        return absl::StrJoin(V, ", ");
    }

    static std::string PrintPI(const Policy &PI) {
        struct PolicyFormatter {
            void operator()(std::string *out, const std::vector<double> &v) const {
                out->append(absl::StrJoin(v, ", "));
            }
        };
        return absl::StrJoin(PI, "\n", PolicyFormatter());
    }

    static std::string PrintQ(const QFunction &Q) {
        struct QFormatter {
            void operator()(std::string *out, const std::vector<double> &Q_s) const {
                out->append(absl::StrJoin(Q_s, ", "));
            }
        };
        return absl::StrJoin(Q, "\n", QFormatter());
    }

    static int Sample(const std::vector<double> &probabilities) {
        absl::BitGen bitGen_;
        double rand = absl::Uniform(bitGen_, 0, 1.0);
        double sum = 0;
        for (int i = 0; i < probabilities.size(); i++) {
            auto const p = probabilities[i];
            if (sum <= rand && rand < (sum + p)) {
                return i;
            }
            sum += p;
        }
        throw std::runtime_error("internal error: failed to sample an action");
    }

    static int EpsilonGreedy(const std::vector<double> &values, double epsilon) {
        absl::BitGen bitGen_;
        auto const r = absl::Uniform(bitGen_, 0.0, 1.0);
        if (r < epsilon) {
            return absl::Uniform(bitGen_, 0, (int) values.size());
        } else {
            return std::distance(values.begin(),
                                 std::max_element(values.begin(), values.end()));
        }
    }

    static int Greedy(const std::vector<double> &values) {
        return std::distance(values.begin(),
                             std::max_element(values.begin(), values.end()));
    }

    enum MonteCarloType {
        FirstVisit,
        EveryVisit
    };
}

#endif //SEB_RL_CPP_ALGORITHMS_H
