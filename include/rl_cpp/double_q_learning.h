//
// Created by sebastian on 30.04.20.
//

#ifndef SEB_RL_CPP_DOUBLE_Q_LEARNING_H
#define SEB_RL_CPP_DOUBLE_Q_LEARNING_H

#include <utility>

#include "environment.h"
#include "algorithms.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class DoubleQLearning : public Algorithm {
        inline static const std::string name = "DoubleQLearning";
        inline static const std::string description = "DoubleQLearning tries to limit the overestimation bias of standard "
                                                      "QLearning which comes from choosing and evaluating an action "
                                                      "with the same Q-function by learning two Q-functions. An action is"
                                                      "chosen using the first Q-function and is then evaluated using the "
                                                      "second Q-function or vice versa.";
        inline static const std::set<Specifications> specifications = {Specifications::CONTROL,
                                                                       Specifications::MODELFREE,
                                                                       Specifications::TEMPORALDIFFERENCE,
                                                                       Specifications::OFFPOLICY};
        inline static const bool providesValueFunction = false;
        inline static const bool providesPolicy = true;
        inline static const bool providesQFunction = true;

    public:
        explicit DoubleQLearning(std::shared_ptr<environment::Environment> environment,
                                 double gamma = kGamma,
                                 double epsilon = kEpsilon,
                                 double alpha = 0.1
        ) : Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                      providesValueFunction, specifications),
            epsilon_(epsilon),
            alpha_(alpha) {
            // initialize q functions with zeroes
            // Q1_ = QFunction(env_->getNumStates(), std::vector(env_->getNumActions(), 0.0));
            Q1_.reserve(env_->getNumStates());
            for (int i = 0; i < env_->getNumStates(); i++) {
                Q1_.emplace_back(std::vector<double>(env_->getNumActions(), 0.0));
            }
            // Q2_ = QFunction(env_->getNumStates(), std::vector(env_->getNumActions(), 0.0));
            Q2_.reserve(env_->getNumStates());
            for (int i = 0; i < env_->getNumStates(); i++) {
                Q2_.emplace_back(std::vector<double>(env_->getNumActions(), 0.0));
            }
        }

        const ValueFunction &V() override;

        const QFunction &Q() override;

        const QFunction &Q1() { return Q1_; };

        const QFunction &Q2() { return Q2_; };

    private:
        void Run() override;

        void Run(int numIterations) override;

        void Run(const environment::Episode &episode) override;

        void Update(const environment::TimeStep &timeStep);

        double epsilon_;
        double alpha_;
        QFunction Q1_;
        QFunction Q2_;
        absl::BitGen bitGen_;
    };
}

#endif //SEB_RL_CPP_DOUBLE_Q_LEARNING_H
