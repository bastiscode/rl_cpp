//
// Created by sebastian on 11.05.20.
//

#ifndef SEB_RL_CPP_GRID_WORLD_H
#define SEB_RL_CPP_GRID_WORLD_H

#include <utility>
#include <variant>

#include "environment.h"
#include "absl/random/random.h"

namespace rl_cpp::grid_world {
    constexpr int kNumActions = 4;

    enum GridTileType {
        TILE,
        GOAL,
        WALL,
        TELEPORTER
    };

    struct Tile {
        int state;

        explicit Tile(int state) : state(state) {};
    };

    struct Goal {
        int state;
        int rewardForReaching;
        const bool terminal = true;

        Goal(int state, int rewardForReaching) : state(state), rewardForReaching(rewardForReaching) {};
    };

    struct Wall {
        int state;

        explicit Wall(int state) : state(state) {};
    };

//    struct Bridge {
//        int fromState;
//        int action;
//        int toState;
//
//        Bridge(int fromState, int action, int toState) :
//                fromState(fromState), action(action), toState(toState) {};
//    };

    enum TeleporterType {
        UNIFORM,
    };

    struct Teleporter {
        int state;
        TeleporterType type;

        Teleporter(int state, TeleporterType type) :
                state(state), type(type) {};
    };

    struct GridStateInfo {
        int row;
        int column;
        GridTileType type;

        GridStateInfo(int row, int column, GridTileType type) :
                row(row), column(column), type(type) {};
    };

    class GridWorldEnvironment : public environment::Environment {
    public:
        GridWorldEnvironment(int gridHeight, int gridWidth);

        GridWorldEnvironment(int gridHeight, int gridWidth,
                             const std::vector<Goal> &goals,
                             const std::vector<Wall> &walls = {},
//                             const std::vector<Bridge> &bridges = {},
                             const std::vector<Teleporter> &teleporters = {});

        [[nodiscard]] GridStateInfo StateInfo(int state) const;

//        [[nodiscard]] bool setGridTile(int state, const std::variant<Tile, Goal, Wall, Teleporter> &gridTile);

        [[nodiscard]] int getGridWidth() const;

        [[nodiscard]] int getGridHeight() const;

        [[nodiscard]] const std::vector<Goal> &getGoals() const;

        [[nodiscard]] const std::vector<Wall> &getWalls() const;

//        [[nodiscard]] const std::vector<Bridge> &getBridges() const;

        [[nodiscard]] const std::vector<Teleporter> &getTeleporters() const;

        [[nodiscard]] std::vector<int> getNeighboringStates(int state);

    protected:

    private:
        std::vector<environment::Transition> SA(int state, int action) override;

        int InitialState() override;


        [[nodiscard]] std::tuple<int, int> rc(int state) const;

//        [[nodiscard]] bool setTile(int state, Tile tile);
//
//        [[nodiscard]] bool setGoal(int state, Goal goal);
//
//        [[nodiscard]] bool setWall(int state, Wall wall);
//
//        [[nodiscard]] bool setTeleporter(int state, Teleporter teleporter);

        std::vector<GridTileType> gridTileTypes_;
        std::vector<Goal> goals_;
        std::vector<Wall> walls_;
//        std::vector<Bridge> bridges_;
        std::vector<Teleporter> teleporters_;
        int gridWidth_;
        int gridHeight_;
        absl::BitGen bitGen_{};
    };
}

#endif //SEB_RL_CPP_GRID_WORLD_H
