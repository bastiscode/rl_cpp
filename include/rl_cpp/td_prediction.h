//
// Created by sebastian on 29.04.20.
//

#ifndef SEB_RL_CPP_TD_PREDICTION_H
#define SEB_RL_CPP_TD_PREDICTION_H

#include <utility>

#include "environment.h"
#include "algorithms.h"

using namespace rl_cpp;

namespace rl_cpp::algorithms {

    class TDPrediction : public Algorithm {
        inline static const std::string name = "TDPrediction";
        inline static const std::string description = "MonteCarloPrediction can be used to evaluate a given policy "
                                                      "by learning the corresponding value function from "
                                                      "single environment steps.";
        inline static const std::set<Specifications> specifications = {Specifications::PREDICTION,
                                                                       Specifications::MODELFREE,
                                                                       Specifications::TEMPORALDIFFERENCE};
        inline static const bool providesValueFunction = true;
        inline static const bool providesPolicy = false;
        inline static const bool providesQFunction = false;

    public:
        explicit TDPrediction(std::shared_ptr<environment::Environment> environment,
                              const Policy &PI,
                              double gamma = kGamma,
                              double alpha = 0.1) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications),
                alpha_(alpha) {
            PI_ = PI;
        };

        explicit TDPrediction(std::shared_ptr<environment::Environment> environment,
                              double gamma = kGamma,
                              double alpha = 0.1) :
                Algorithm(std::move(environment), gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications), alpha_(alpha) {
        };

    private:
        void Run() override;

        void Run(int numIterations) override;

        double alpha_;
    };
}

#endif //SEB_RL_CPP_TD_PREDICTION_H
