//
// Created by sebastian on 22.04.20.
//

#ifndef SEB_RL_CPP_ENVIRONMENT_H
#define SEB_RL_CPP_ENVIRONMENT_H

#include <vector>
#include <stdexcept>

namespace rl_cpp::environment {
    struct Transition {
        double transitionProb;
        int state;
        double reward;
        bool terminal;

        Transition(double transitionProb, int state, double reward, bool terminal) :
                transitionProb(transitionProb), state(state), reward(reward), terminal(terminal) {};
    };

    struct TimeStep {
        int state;
        int action;
        double reward;
        int nextState;
        bool terminal;

        TimeStep(int state, int action, double reward, int nextState, bool terminal) :
                state(state), action(action), reward(reward), nextState(nextState), terminal(terminal) {};
    };

    using Episode = std::vector<TimeStep>;

    class Environment {
    public:
        Environment() : numStates(0), numActions(0) {};

        virtual ~Environment() = default;

        virtual std::vector<Transition> SA(int state, int action) = 0;

        virtual int InitialState() = 0;

        virtual Episode EpisodeFromStates(const std::vector<int> &states) {
            throw std::logic_error("Not implemented");
        }

        [[nodiscard]] int getNumStates() const { return numStates; };

        [[nodiscard]] int getNumActions() const { return numActions; };

    protected:
        int numStates;
        int numActions;
        std::vector<std::vector<std::vector<Transition>>> stateActionMatrix; // states X actions X possible transitions
    };
}
#endif //SEB_RL_CPP_ENVIRONMENT_H
