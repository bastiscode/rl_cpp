//
// Created by sebastian on 25.04.20.
//

#ifndef SEB_RL_CPP_VALUE_ITERATION_H
#define SEB_RL_CPP_VALUE_ITERATION_H

#include "environment.h"
#include "algorithms.h"

#include <memory>
#include <utility>

using namespace rl_cpp;

namespace rl_cpp::algorithms {
    class ValueIteration : public Algorithm {
        inline static const std::string name = "ValueIteration";

        inline static const std::string description = "ValueIteration finds the optimal value function using the Bellman value optimality equation, "
                                                      "then extracts the best (greedy) policy from it. "
                                                      "We can do this because we have a model of the environment. "
                                                      "If you dont have a model you would need to use other algorithms which can "
                                                      "extract a policy from Q-values in the model-free way.";
        inline static const std::set<Specifications> specifications = {Specifications::CONTROL,
                                                                       Specifications::MODELBASED};
        inline static const bool providesValueFunction = true;
        inline static const bool providesPolicy = true;
        inline static const bool providesQFunction = false;

    public:
        explicit ValueIteration(const std::shared_ptr<environment::Environment> &environment,
                                double gamma = algorithms::kGamma,
                                int maxIterations = algorithms::kMaxIterations,
                                double maxDelta = algorithms::kMaxDelta) :
                Algorithm(environment, gamma, name, description, providesQFunction, providesPolicy,
                          providesValueFunction, specifications),
                maxIterations_(maxIterations),
                maxDelta_(maxDelta) {};

    private:

        void PolicyEvaluation();

        void PolicyExtraction();

        void Run() override;

        void Run(int numIterations) override;

        double maxDelta_ = kMaxDelta;
        int maxIterations_ = kMaxIterations;
    };
};

#endif //SEB_RL_CPP_VALUE_ITERATION_H
